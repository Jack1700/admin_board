import 'package:admin_ui/model/user.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

abstract class ServiceApi {
  Future<List<User>> getUsers();
  Future<User> getUser(int id);
  Future<User> createUser(User user);
  Future<User> updateUser(User user);
  Future<bool> deleteUser(String id);
}

// Our ServiceApi class to communicate with the server with basic CRUD operations

class UserService extends ServiceApi {
  String BASE_URL = "http://localhost:8000";
  @override
  Future<List<User>> getUsers() async {
    try {
      var uri = Uri.parse(BASE_URL + "/user");
      var response =
          await http.get(uri, headers: {"ContentType": "application/json"});
      if (response.statusCode == 200) {
        List<dynamic> jsonResponse = jsonDecode(response.body);
        List<User> list = [];

        for (var item in jsonResponse) {
          var _user = User.fromJson(item);
          list.add(_user);
        }
        return list;
      } else {
        throw Exception('Failed to load users');
      }
    } catch (e) {
      print(e);
      return List<User>.empty();
    }
  }

  @override
  Future<User> createUser(User user) async {
    try {
      var uri = Uri.parse(BASE_URL + "/user");
      var response = await http.post(uri,
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
          },
          body: jsonEncode(<String, String>{
            'name': user.name,
            'email': user.email,
            'password': user.password!,
            'roles': json.encode(user.roles),
          }));
      if (response.statusCode == 200) {
        return User.fromJson(jsonDecode(response.body));
      } else {
        throw Exception('Failed to create a user');
      }
    } catch (e) {
      print(e.toString());
      throw Exception('Failed create a user');
    }
  }

  @override
  Future<bool> deleteUser(String id) async {
    try {
      var uri = Uri.parse(BASE_URL + "/user/$id");
      var response =
          await http.delete(uri, headers: {"ContentType": "application/json"});
      if (response.statusCode == 200) {
        return true;
      } else {
        throw Exception('Failed to delete a user');
      }
    } catch (e) {
      print(e.toString());
      throw Exception('Failed to delete a user');
    }
  }

  @override
  Future<User> getUser(int id) async {
    try {
      var uri = Uri.parse(BASE_URL + "/user/$id");
      var response =
          await http.get(uri, headers: {"ContentType": "application/json"});
      if (response.statusCode == 200) {
        var jsonResponse = jsonDecode(response.body);
        return User.fromJson(jsonResponse);
      } else {
        throw Exception('Failed to load a user');
      }
    } catch (e) {
      print(e.toString());
      throw Exception('Failed to load a user');
    }
  }

  @override
  Future<User> updateUser(User user) async {
    try {
      var uri = Uri.parse(BASE_URL + "/user/${user.id}");
      var response = await http.put(uri,
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
          },
          body: jsonEncode(<String, String>{
            'name': user.name,
            'email': user.email,
            'roles': json.encode(user.roles)
          }));
      if (response.statusCode == 200) {
        return User.fromJson(jsonDecode(response.body));
      } else {
        throw Exception('Failed to update a user');
      }
    } catch (e) {
      print(e.toString());
      throw Exception('Failed to update a user');
    }
  }
}

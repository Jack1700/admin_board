import 'package:admin_ui/model/role.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

abstract class ServiceApi {
  Future<List<Role>> getRoles();
  Future<Role> getRole(int id);
  Future<Role> createRole(Role role);
  Future<Role> updateRole(Role role);
  Future<bool> deleteRole(String id);
}

class RoleService extends ServiceApi {
  String BASE_URL = "http://localhost:8000";
  @override
  Future<List<Role>> getRoles() async {
    try {
      var uri = Uri.parse(BASE_URL + "/role");
      var response =
          await http.get(uri, headers: {"ContentType": "application/json"});
      if (response.statusCode == 200) {
        List<dynamic> jsonResponse = jsonDecode(response.body);
        List<Role> list = [];

        for (var item in jsonResponse) {
          var _role = Role.fromJson(item);
          list.add(_role);
        }
        return list;
      } else {
        throw Exception('Failed to load roles');
      }
    } catch (e) {
      print(e);
      return List<Role>.empty();
    }
  }

  @override
  Future<Role> createRole(Role role) async {
    try {
      var ruleIdArray = role.rules.map((x) => x.id).toList();
      var uri = Uri.parse(BASE_URL + "/role");
      var response = await http.post(uri,
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
          },
          body: jsonEncode(<String, String>{
            'name': role.name,
            'description': role.description,
            'rules': json.encode(role.rules)
          }));
      if (response.statusCode == 200) {
        return Role.fromJson(jsonDecode(response.body));
      } else {
        throw Exception('Failed to create a role');
      }
    } catch (e) {
      print(e.toString());
      throw Exception('Failed create a role');
    }
  }

  @override
  Future<bool> deleteRole(String id) async {
    try {
      var uri = Uri.parse(BASE_URL + "/role/$id");
      var response =
          await http.delete(uri, headers: {"ContentType": "application/json"});
      if (response.statusCode == 200) {
        return true;
      } else {
        throw Exception('Failed to delete a role');
      }
    } catch (e) {
      print(e.toString());
      throw Exception('Failed to delete a role');
    }
  }

  @override
  Future<Role> getRole(int id) async {
    try {
      var uri = Uri.parse(BASE_URL + "/role/$id");
      var response =
          await http.get(uri, headers: {"ContentType": "application/json"});
      if (response.statusCode == 200) {
        var jsonResponse = jsonDecode(response.body);
        return Role.fromJson(jsonResponse);
      } else {
        throw Exception('Failed to load a role');
      }
    } catch (e) {
      print(e.toString());
      throw Exception('Failed to load a role');
    }
  }

  @override
  Future<Role> updateRole(Role role) async {
    try {
      var uri = Uri.parse(BASE_URL + "/role/${role.id}");
      var response = await http.put(uri,
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
          },
          body: jsonEncode(<String, String>{
            'name': role.name,
            'description': role.description,
            'rules': json.encode(role.rules)
          }));
      if (response.statusCode == 200) {
        return Role.fromJson(jsonDecode(response.body));
      } else {
        throw Exception('Failed to update a role');
      }
    } catch (e) {
      print(e.toString());
      throw Exception('Failed to update a role');
    }
  }
}

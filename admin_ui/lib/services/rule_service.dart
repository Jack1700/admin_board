import 'package:admin_ui/model/rule.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

abstract class ServiceApi {
  Future<List<Rule>> getRules();
  Future<Rule> getRule(int id);
  Future<Rule> createRule(Rule rule);
  Future<Rule> updateRule(Rule rule);
  Future<bool> deleteRule(String id);
}

class RuleService extends ServiceApi {
  String BASE_URL = "http://localhost:8000";
  @override
  Future<List<Rule>> getRules() async {
    try {
      var uri = Uri.parse(BASE_URL + "/rule");
      var response =
          await http.get(uri, headers: {"ContentType": "application/json"});
      if (response.statusCode == 200) {
        var jsonResponse = jsonDecode(response.body);
        print(jsonResponse);
        var l = List<Rule>.from(jsonResponse.map((x) => Rule.fromMap(x)));
        return l;
      } else {
        throw Exception('Failed to load rules');
      }
    } catch (e) {
      return List<Rule>.empty();
    }
  }

  @override
  Future<Rule> createRule(Rule rule) async {
    try {
      var uri = Uri.parse(BASE_URL + "/rule");
      var response = await http.post(uri,
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
          },
          body: jsonEncode(<String, String>{
            'name': rule.name,
            'description': rule.description,
          }));
      if (response.statusCode == 200) {
        return Rule.fromJson(response.body);
      } else {
        throw Exception('Failed to create a rule');
      }
    } catch (e) {
      print(e.toString());
      throw Exception('Failed create a rule');
    }
  }

  @override
  Future<bool> deleteRule(String id) async {
    try {
      var uri = Uri.parse(BASE_URL + "/rule/$id");
      var response =
          await http.delete(uri, headers: {"ContentType": "application/json"});
      if (response.statusCode == 200) {
        return true;
      } else {
        throw Exception('Failed to delete a rule');
      }
    } catch (e) {
      print(e.toString());
      throw Exception('Failed to delete a rule');
    }
  }

  @override
  Future<Rule> getRule(int id) async {
    try {
      var uri = Uri.parse(BASE_URL + "/rule/$id");
      var response =
          await http.get(uri, headers: {"ContentType": "application/json"});
      if (response.statusCode == 200) {
        var jsonResponse = jsonDecode(response.body);
        return Rule.fromJson(jsonResponse);
      } else {
        throw Exception('Failed to load a rule');
      }
    } catch (e) {
      print(e.toString());
      throw Exception('Failed to load a rule');
    }
  }

  @override
  Future<Rule> updateRule(Rule rule) async {
    try {
      var uri = Uri.parse(BASE_URL + "/rule/${rule.id}");
      var map = rule.toMap();
      var response = await http.put(uri,
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
          },
          body: jsonEncode(map));
      if (response.statusCode == 200) {
        return Rule.fromJson(response.body);
      } else {
        throw Exception('Failed to update a rule');
      }
    } catch (e) {
      print(e.toString());
      throw Exception('Failed to update a rule');
    }
  }
}

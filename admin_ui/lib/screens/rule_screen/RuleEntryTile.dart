import 'package:admin_ui/bloc/rule_bloc.dart';
import 'package:admin_ui/model/rule.dart';
import 'package:fluent_ui/fluent_ui.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'RuleEditDialog.dart';

class EntryTile extends StatelessWidget {
  final Rule rule;

  EntryTile({
    required this.rule,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(8),
      child: ListTile(
        tileColor: Colors.magenta,
        title: Text(rule.name.toString()),
        subtitle: Text(rule.description.toString()),
        trailing: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            TextButton(
              child: const Text('Edit', style: TextStyle(color: Colors.white)),
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (context) => RuleEditDialog(rule: rule),
                );
              },
            ),
            TextButton(
              child:
                  const Text('Delete', style: TextStyle(color: Colors.white)),
              onPressed: () async {
                var shouldDelete = await showDialog<bool>(
                  context: context,
                  builder: (context) => ContentDialog(
                    title: const Text('Delete entry?'),
                    actions: [
                      TextButton(
                        child: const Text('Cancel'),
                        onPressed: () => Navigator.of(context).pop(false),
                      ),
                      TextButton(
                        child: const Text('Delete'),
                        onPressed: () => Navigator.of(context).pop(true),
                      ),
                    ],
                  ),
                );
                if (shouldDelete != null && shouldDelete == true) {
                  BlocProvider.of<RuleBloc>(context).add(DeleteRule(rule));
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}

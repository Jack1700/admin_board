import 'package:admin_ui/bloc/rule_bloc.dart';
import 'package:admin_ui/components/LoadingIndicator.dart';
import 'package:admin_ui/screens/rule_screen/RuleEntryTile.dart';
import 'package:flutter/material.dart' as material;
import 'package:fluent_ui/fluent_ui.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

// A Simple SCreen with a list of all roules, the option to modify, delete and a button to add a new rule

class RuleList extends StatefulWidget {
  const RuleList({material.Key? key}) : super(key: key);

  @override
  _RuleListState createState() => _RuleListState();
}

class _RuleListState extends State<RuleList> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      bloc: BlocProvider.of<RuleBloc>(context),
      builder: (material.BuildContext context, RuleState state) {
        if (state is RuleInitialState || state is RuleLoadingState) {
          return const LoadingIndicator();
        }
        if (state is RuleListErrorstate) {
          return const Center(
            child: Text('failed to fetch posts'),
          );
        }
        if (state is RuleLoadedState) {
          return ListView.builder(
            itemBuilder: (context, index) {
              return EntryTile(
                rule: state.rules[index],
              );
            },
            itemCount: state.rules.length,
          );
        } else {
          return Container();
        }
      },
    );
  }
}

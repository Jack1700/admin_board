import 'package:admin_ui/bloc/rule_bloc.dart';
import 'package:admin_ui/model/rule.dart';
import 'package:fluent_ui/fluent_ui.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class RuleEditDialog extends StatefulWidget {
  final Rule rule;

  const RuleEditDialog({Key? key, required this.rule}) : super(key: key);

  @override
  State<RuleEditDialog> createState() => _RuleEditDialogState();
}

class _RuleEditDialogState extends State<RuleEditDialog> {
  final _formKey = GlobalKey<FormState>();
  final _nameController = TextEditingController();
  final _descriptionController = TextEditingController();

  @override
  void dispose() {
    _nameController.dispose();
    _descriptionController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _nameController.text = widget.rule.name;
    _descriptionController.text = widget.rule.description;
    return ContentDialog(
      title: const Text('Edit rule'),
      content: Form(
        key: _formKey,
        child: Column(
          children: [
            TextFormBox(
                controller: _nameController,
                autovalidateMode: AutovalidateMode.always,
                validator: (text) {
                  if (text == null || text.isEmpty) {
                    return 'Provide an name for the rule';
                  }
                }),
            TextFormBox(
                controller: _descriptionController,
                autovalidateMode: AutovalidateMode.always,
                validator: (text) {
                  if (text == null || text.isEmpty) {
                    return 'Provide an description for the rule';
                  }
                }),
          ],
        ),
      ),
      actions: [
        Button(
          child: const Text('Cancel'),
          onPressed: () {
            // Dismiss the dialog
            Navigator.of(context).pop();
          },
        ),
        // Disable the button if the form is invalid
        Button(
          child: const Text('Save'),
          onPressed: () {
            // Validate and save the entry
            if (_formKey.currentState!.validate()) {
              Rule rule = Rule(
                id: widget.rule.id,
                name: _nameController.text,
                description: _descriptionController.text,
              );
              BlocProvider.of<RuleBloc>(context).add(UpdateRule(rule));
              // Dismiss the dialog
              Navigator.of(context).pop(true);
            }
          },
        ),
      ],
    );
  }
}

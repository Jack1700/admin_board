import 'package:admin_ui/bloc/rule_bloc.dart';
import 'package:admin_ui/model/rule.dart';
import 'package:fluent_ui/fluent_ui.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class RuleCreateDialog extends StatefulWidget {
  const RuleCreateDialog({Key? key}) : super(key: key);

  @override
  State<RuleCreateDialog> createState() => _RuleCreateDialogState();
}

class _RuleCreateDialogState extends State<RuleCreateDialog> {
  final _formKey = GlobalKey<FormState>();
  final _nameController = TextEditingController();
  final _descriptionController = TextEditingController();

  @override
  void dispose() {
    _nameController.dispose();
    _descriptionController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ContentDialog(
      title: const Text('Create rule'),
      content: Form(
        key: _formKey,
        child: Column(
          children: [
            TextFormBox(
                autovalidateMode: AutovalidateMode.always,
                controller: _nameController,
                validator: (text) {
                  if (text == null || text.isEmpty) {
                    return 'Provide an name for the rule';
                  }
                  return null;
                }),
            TextFormBox(
                autovalidateMode: AutovalidateMode.always,
                controller: _descriptionController,
                validator: (text) {
                  if (text == null || text.isEmpty) {
                    return 'Provide an description for the rule';
                  }
                  return null;
                }),
          ],
        ),
      ),
      actions: [
        Button(
          child: const Text('Cancel'),
          onPressed: () {
            // Dismiss the dialog
            Navigator.of(context).pop(false);
          },
        ),
        // Disable the button if the form is invalid
        Button(
          child: const Text('Save'),
          onPressed: () {
            // Validate and save the entry
            if (_formKey.currentState!.validate()) {
              // Dismiss the dialog
              Rule rule = Rule(
                name: _nameController.text,
                description: _descriptionController.text,
              );
              BlocProvider.of<RuleBloc>(context).add(CreateRule(rule));
              Navigator.of(context).pop(true);
            }
          },
        ),
      ],
    );
  }
}

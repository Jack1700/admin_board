import 'package:admin_ui/screens/rule_screen/RuleCreateDialog.dart';
import 'package:admin_ui/screens/rule_screen/RuleList.dart';
import 'package:fluent_ui/fluent_ui.dart';
import 'package:flutter/material.dart' as material;

class RuleMain extends StatelessWidget {
  const RuleMain({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScaffoldPage(
      header: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const Padding(
            padding: EdgeInsets.only(left: 16),
            child: Text(
              "Rules",
              style: TextStyle(fontSize: 48),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 16),
            child: material.ElevatedButton(
              child: const Text("Add"),
              onPressed: () {
                showDialog<bool>(
                  context: context,
                  builder: (context) => const RuleCreateDialog(),
                );
              },
            ),
          )
        ],
      ),
      content: Container(
        padding: const EdgeInsets.all(16),
        child: const Center(
          child: RuleList(),
        ),
      ),
    );
  }
}

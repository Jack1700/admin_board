import 'package:admin_ui/screens/role_screen/RoleCreateDialog.dart';
import 'package:admin_ui/screens/role_screen/RoleList.dart';
import 'package:fluent_ui/fluent_ui.dart';
import 'package:flutter/material.dart' as material;

class RoleMain extends StatelessWidget {
  const RoleMain({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScaffoldPage(
      header: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const Padding(
            padding: EdgeInsets.only(left: 16),
            child: Text(
              "Roles",
              style: TextStyle(fontSize: 48),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 16),
            child: material.ElevatedButton(
              child: const Text("Add"),
              onPressed: () {
                showDialog<bool>(
                  context: context,
                  builder: (context) => const RoleCreateDialog(),
                );
              },
            ),
          )
        ],
      ),
      content: Container(
        padding: const EdgeInsets.all(16),
        child: const Center(
          child: RoleList(),
        ),
      ),
    );
  }
}

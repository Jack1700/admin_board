import 'package:admin_ui/bloc/role_bloc.dart';
import 'package:admin_ui/components/LoadingIndicator.dart';
import 'package:admin_ui/screens/role_screen/RoleEntryTile.dart';
import 'package:flutter/material.dart' as material;
import 'package:fluent_ui/fluent_ui.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

// A Simple SCreen with a list of all roules, the option to modify, delete and a button to add a new role

class RoleList extends StatefulWidget {
  const RoleList({material.Key? key}) : super(key: key);

  @override
  _RoleListState createState() => _RoleListState();
}

class _RoleListState extends State<RoleList> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      bloc: BlocProvider.of<RoleBloc>(context),
      builder: (material.BuildContext context, RoleState state) {
        if (state is RoleInitialState || state is RoleLoadingState) {
          return const LoadingIndicator();
        }
        if (state is RoleListErrorstate) {
          return const Center(
            child: Text('failed to fetch posts'),
          );
        }
        if (state is RoleLoadedState) {
          return ListView.builder(
            itemBuilder: (context, index) {
              return RoleEntryTile(
                role: state.roles[index],
              );
            },
            itemCount: state.roles.length,
          );
        } else {
          return Container();
        }
      },
    );
  }
}

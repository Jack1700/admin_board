import 'package:admin_ui/bloc/role_bloc.dart';
import 'package:admin_ui/bloc/rule_bloc.dart';
import 'package:admin_ui/components/LoadingIndicator.dart';
import 'package:admin_ui/model/role.dart';
import 'package:admin_ui/model/rule.dart';
import 'package:admin_ui/screens/role_screen/RoleCustomChip.dart';
import 'package:fluent_ui/fluent_ui.dart';
import 'package:flutter/material.dart' as material;
import 'package:flutter_bloc/flutter_bloc.dart';

class RoleCreateDialog extends StatefulWidget {
  const RoleCreateDialog({Key? key}) : super(key: key);

  @override
  State<RoleCreateDialog> createState() => _RoleCreateDialogState();
}

class _RoleCreateDialogState extends State<RoleCreateDialog> {
  final _formKey = GlobalKey<FormState>();
  final _nameController = TextEditingController();
  final _descriptionController = TextEditingController();
  final List<Rule> _selectedRules = [];

  @override
  void dispose() {
    _nameController.dispose();
    _descriptionController.dispose();
    super.dispose();
  }

  onSelection(bool selected, Rule rule) {
    setState(() {
      if (selected) {
        _selectedRules.add(rule);
      } else {
        _selectedRules.remove(rule);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return ContentDialog(
      title: const Text('Create role'),
      content: Form(
        key: _formKey,
        child: Column(
          children: [
            Container(
              padding: const EdgeInsets.only(bottom: 8),
              child: TextFormBox(
                  header: "The Name of the Role:",
                  autovalidateMode: AutovalidateMode.always,
                  controller: _nameController,
                  validator: (text) {
                    if (text == null || text.isEmpty) {
                      return 'Provide an name for the role';
                    }
                    return null;
                  }),
            ),
            Container(
              padding: const EdgeInsets.only(top: 8, bottom: 8),
              child: TextFormBox(
                  header: "The Description of the Role:",
                  autovalidateMode: AutovalidateMode.always,
                  controller: _descriptionController,
                  validator: (text) {
                    if (text == null || text.isEmpty) {
                      return 'Provide an description for the role';
                    }
                    return null;
                  }),
            ),
            // A multiselect list of permissions
            Column(
              children: [
                Container(
                  padding: const EdgeInsets.only(top: 8, bottom: 4),
                  alignment: Alignment.centerLeft,
                  child: const Text("Select Rules:"),
                ),
                SizedBox(
                  height: 100,
                  width: 380,
                  child: material.Card(
                    child: BlocBuilder(
                      bloc: BlocProvider.of<RuleBloc>(context),
                      builder: (context, state) {
                        if (state is RuleLoadingState) {
                          return const LoadingIndicator();
                        } else if (state is RuleListErrorstate) {
                          return const LoadingIndicator();
                        } else if (state is RuleLoadedState) {
                          return Wrap(
                            direction: Axis.horizontal,
                            children: state.rules
                                .map((rule) => RoleCustomChip(
                                    rule: rule,
                                    isSelected: _selectedRules.contains(rule),
                                    onSelection: onSelection))
                                .toList(),
                          );
                        } else {
                          return const LoadingIndicator();
                        }
                      },
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
      actions: [
        Button(
          child: const Text('Cancel'),
          onPressed: () {
            // Dismiss the dialog
            Navigator.of(context).pop(false);
          },
        ),
        // Disable the button if the form is invalid
        Button(
          child: const Text('Save'),
          onPressed: () {
            // Validate and save the entry
            if (_formKey.currentState!.validate()) {
              // Dismiss the dialog
              Role role = Role(
                name: _nameController.text,
                description: _descriptionController.text,
                rules: _selectedRules,
              );
              BlocProvider.of<RoleBloc>(context).add(CreateRole(role));
              Navigator.of(context).pop(true);
            }
          },
        ),
      ],
    );
  }
}

import 'package:admin_ui/model/rule.dart';
import 'package:fluent_ui/fluent_ui.dart';
import 'package:flutter/material.dart' as material;

class RoleCustomChip extends StatelessWidget {
  const RoleCustomChip({
    Key? key,
    required this.rule,
    required this.isSelected,
    required this.onSelection,
  }) : super(key: key);

  final Rule rule;
  final bool isSelected;
  final Function onSelection;

  @override
  Widget build(BuildContext context) {
    return material.Container(
      padding: const EdgeInsets.all(3),
      child: material.FilterChip(
        selected: isSelected,
        backgroundColor: Colors.teal,
        avatar: material.CircleAvatar(
          backgroundColor: Colors.blue,
          child: Text(
            rule.name[0].toUpperCase(),
            style: const TextStyle(color: Colors.white),
          ),
        ),
        label: Text(
          rule.name,
        ),
        selectedColor: Colors.purple,
        onSelected: (bool selected) {
          onSelection(selected, rule);
        },
      ),
    );
  }
}

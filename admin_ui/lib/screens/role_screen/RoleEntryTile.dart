import 'package:admin_ui/bloc/role_bloc.dart';
import 'package:admin_ui/bloc/rule_bloc.dart';
import 'package:admin_ui/components/LoadingIndicator.dart';
import 'package:admin_ui/model/role.dart';
import 'package:admin_ui/screens/role_screen/RoleCustomChip.dart';
import 'package:fluent_ui/fluent_ui.dart';
import 'package:flutter/material.dart' as material;
import 'package:flutter_bloc/flutter_bloc.dart';

import 'RoleEditDialog.dart';

class RoleEntryTile extends StatelessWidget {
  final Role role;

  RoleEntryTile({
    required this.role,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(8),
      child: ListTile(
        tileColor: Colors.yellow,
        title: Text(
          role.name.toString(),
          style: TextStyle(color: Colors.black),
        ),
        subtitle: Text(
          role.description.toString(),
          style: TextStyle(color: Colors.black),
        ),
        trailing: Row(
          children: [
            Container(
              margin: EdgeInsets.only(right: 24),
              child: Wrap(
                direction: Axis.horizontal,
                children: role.rules
                    .map((rule) => Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.grey[200],
                          ),
                          margin: const EdgeInsets.only(left: 3, right: 3),
                          padding: const EdgeInsets.only(
                              left: 8, right: 8, top: 4, bottom: 4),
                          child: Text(rule.name),
                        ))
                    .toList(),
              ),
            ),
            Container(
              margin: EdgeInsets.only(right: 8),
              child: material.ElevatedButton(
                child:
                    const Text('Edit', style: TextStyle(color: Colors.black)),
                onPressed: () {
                  showDialog(
                    context: context,
                    builder: (context) => RoleEditDialog(role: role),
                  );
                },
              ),
            ),
            material.ElevatedButton(
              child:
                  const Text('Delete', style: TextStyle(color: Colors.black)),
              onPressed: () async {
                var shouldDelete = await showDialog<bool>(
                  context: context,
                  builder: (context) => ContentDialog(
                    title: const Text('Delete entry?'),
                    actions: [
                      TextButton(
                        child: const Text('Cancel'),
                        onPressed: () => Navigator.of(context).pop(false),
                      ),
                      TextButton(
                        child: const Text('Delete'),
                        onPressed: () => Navigator.of(context).pop(true),
                      ),
                    ],
                  ),
                );
                if (shouldDelete != null && shouldDelete == true) {
                  BlocProvider.of<RoleBloc>(context).add(DeleteRole(role));
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}

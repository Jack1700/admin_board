import 'package:admin_ui/bloc/user_bloc.dart';
import 'package:admin_ui/components/LoadingIndicator.dart';
import 'package:admin_ui/screens/user_screen/UserEntryTile.dart';
import 'package:flutter/material.dart' as material;
import 'package:fluent_ui/fluent_ui.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

// A Simple SCreen with a list of all roules, the option to modify, delete and a button to add a new user

class UserList extends StatefulWidget {
  const UserList({material.Key? key}) : super(key: key);

  @override
  _UserListState createState() => _UserListState();
}

class _UserListState extends State<UserList> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      bloc: BlocProvider.of<UserBloc>(context),
      builder: (material.BuildContext context, UserState state) {
        if (state is UserInitialState || state is UserLoadingState) {
          return const LoadingIndicator();
        }
        if (state is UserListErrorstate) {
          return const Center(
            child: Text('failed to fetch posts'),
          );
        }
        if (state is UserLoadedState) {
          return ListView.builder(
            itemBuilder: (context, index) {
              return UserEntryTile(
                user: state.users[index],
              );
            },
            itemCount: state.users.length,
          );
        } else {
          return Container();
        }
      },
    );
  }
}

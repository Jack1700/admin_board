import 'package:fluent_ui/fluent_ui.dart';
import 'package:flutter/material.dart' as material;
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:admin_ui/bloc/role_bloc.dart';
import 'package:admin_ui/bloc/user_bloc.dart';
import 'package:admin_ui/components/LoadingIndicator.dart';
import 'package:admin_ui/model/role.dart';
import 'package:admin_ui/model/role.dart';
import 'package:admin_ui/model/user.dart';
import 'package:admin_ui/screens/user_screen/UserCustomChip.dart';

class UserEditDialog extends StatefulWidget {
  final User user;
  List<Role> _selectedRoles = [];

  UserEditDialog({
    Key? key,
    required this.user,
  }) {
    _selectedRoles = user.roles;
  }

  @override
  State<UserEditDialog> createState() => _UserEditDialogState();
}

class _UserEditDialogState extends State<UserEditDialog> {
  final _formKey = GlobalKey<FormState>();
  final _nameController = TextEditingController();
  final _emailController = TextEditingController();
  List<Role> _selectedRoles = [];

  @override
  void initState() {
    super.initState();
    _selectedRoles = [...widget.user.roles];
    _nameController.text = widget.user.name;
    _emailController.text = widget.user.email;
  }

  @override
  void dispose() {
    _nameController.dispose();
    _emailController.dispose();
    super.dispose();
  }

  // Our form is very simple, so we don't need to do anything special to save
  onSelection(bool selected, Role role) {
    setState(() {
      if (selected) {
        _selectedRoles.add(role);
      } else {
        _selectedRoles.removeWhere((element) => element.id == role.id);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return ContentDialog(
      title: const Text('Edit User'),
      content: Form(
        key: _formKey,
        child: Column(
          children: [
            TextFormBox(
                header: "The Name of the User:",
                controller: _nameController,
                autovalidateMode: AutovalidateMode.always,
                validator: (text) {
                  if (text == null || text.isEmpty) {
                    return 'Provide an name for the user';
                  }
                }),
            TextFormBox(
                header: "The Email of the User:",
                controller: _emailController,
                autovalidateMode: AutovalidateMode.always,
                validator: (text) {
                  if (text == null || text.isEmpty) {
                    return 'Provide the Email for the user';
                  }
                }),
            Column(children: [
              Container(
                padding: const EdgeInsets.only(top: 8, bottom: 4),
                alignment: Alignment.centerLeft,
                child: const Text("Select Roles:"),
              ),
              SizedBox(
                height: 100,
                width: 380,
                child: material.Card(
                  child: BlocBuilder(
                    bloc: BlocProvider.of<RoleBloc>(context),
                    builder: (context, state) {
                      if (state is RoleLoadingState) {
                        return const LoadingIndicator();
                      } else if (state is RoleListErrorstate) {
                        return const LoadingIndicator();
                      } else if (state is RoleLoadedState) {
                        return Wrap(
                          direction: Axis.horizontal,
                          children: state.roles
                              .map((role) => UserCustomChip(
                                  role: role,
                                  isSelected: _selectedRoles
                                      .where(
                                          ((element) => element.id == role.id))
                                      .isNotEmpty,
                                  mySelection: onSelection))
                              .toList(),
                        );
                      } else {
                        return const LoadingIndicator();
                      }
                    },
                  ),
                ),
              ),
            ]),
          ],
        ),
      ),
      actions: [
        Button(
          child: const Text('Cancel'),
          onPressed: () {
            // Reset the form to initial values.
            setState(() {
              _selectedRoles = widget.user.roles;
            });
            // Dismiss the dialog
            Navigator.of(context).pop();
          },
        ),
        // Disable the button if the form is invalid
        Button(
          child: const Text('Save'),
          onPressed: () {
            // Validate and save the entry
            if (_formKey.currentState != null &&
                _formKey.currentState!.validate()) {
              User user = User(
                id: widget.user.id,
                name: _nameController.text,
                email: _emailController.text,
                roles: _selectedRoles,
              );
              // Add Update User to the Bloc as an event to update the user
              BlocProvider.of<UserBloc>(context).add(UpdateUser(user));
              // Dismiss the dialog
              Navigator.of(context).pop(true);
            }
          },
        ),
      ],
    );
  }
}

import 'package:admin_ui/bloc/user_bloc.dart';
import 'package:admin_ui/bloc/role_bloc.dart';
import 'package:admin_ui/components/LoadingIndicator.dart';
import 'package:admin_ui/model/user.dart';
import 'package:admin_ui/screens/user_screen/UserCustomChip.dart';
import 'package:fluent_ui/fluent_ui.dart';
import 'package:flutter/material.dart' as material;
import 'package:flutter_bloc/flutter_bloc.dart';

import 'UserEditDialog.dart';

class UserEntryTile extends StatelessWidget {
  final User user;

  const UserEntryTile({
    required this.user,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(8),
      child: ListTile(
        tileColor: Colors.green,
        title: Text(
          user.name.toString(),
          style: const TextStyle(color: Colors.black),
        ),
        subtitle: Text(
          user.email.toString(),
          style: const TextStyle(color: Colors.black),
        ),
        trailing: Row(
          children: [
            // The user's role
            Container(
              margin: const EdgeInsets.only(right: 24),
              child: Wrap(
                direction: Axis.horizontal,
                children: user.roles
                    .map((role) => Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.grey[200],
                          ),
                          margin: const EdgeInsets.only(left: 3, right: 3),
                          padding: const EdgeInsets.only(
                              left: 8, right: 8, top: 4, bottom: 4),
                          child: Text(role.name),
                        ))
                    .toList(),
              ),
            ),
            // The Action buttons
            Container(
              margin: const EdgeInsets.only(right: 8),
              child: material.ElevatedButton(
                style: material.ElevatedButton.styleFrom(
                  primary: Colors.grey,
                ),
                child:
                    const Text('Edit', style: TextStyle(color: Colors.white)),
                onPressed: () {
                  showDialog(
                    context: context,
                    builder: (context) => UserEditDialog(user: user),
                  );
                },
              ),
            ),
            material.ElevatedButton(
              style: material.ElevatedButton.styleFrom(
                primary: Colors.grey,
              ),
              child:
                  const Text('Delete', style: TextStyle(color: Colors.white)),
              onPressed: () async {
                var shouldDelete = await showDialog<bool>(
                  context: context,
                  builder: (context) => ContentDialog(
                    title: const Text('Delete entry?'),
                    actions: [
                      TextButton(
                        child: const Text('Cancel'),
                        onPressed: () => Navigator.of(context).pop(false),
                      ),
                      TextButton(
                        child: const Text('Delete'),
                        onPressed: () => Navigator.of(context).pop(true),
                      ),
                    ],
                  ),
                );
                if (shouldDelete != null && shouldDelete == true) {
                  BlocProvider.of<UserBloc>(context).add(DeleteUser(user));
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}

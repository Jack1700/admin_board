import 'package:admin_ui/bloc/user_bloc.dart';
import 'package:admin_ui/bloc/role_bloc.dart';
import 'package:admin_ui/components/LoadingIndicator.dart';
import 'package:admin_ui/model/user.dart';
import 'package:admin_ui/model/role.dart';
import 'package:admin_ui/screens/user_screen/UserCustomChip.dart';
import 'package:fluent_ui/fluent_ui.dart';
import 'package:flutter/material.dart' as material;
import 'package:flutter_bloc/flutter_bloc.dart';

class UserCreateDialog extends StatefulWidget {
  const UserCreateDialog({Key? key}) : super(key: key);

  @override
  State<UserCreateDialog> createState() => _UserCreateDialogState();
}

class _UserCreateDialogState extends State<UserCreateDialog> {
  final _formKey = GlobalKey<FormState>();
  final _nameController = TextEditingController();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  final List<Role> _selectedRoles = [];

  @override
  void dispose() {
    _nameController.dispose();
    _emailController.dispose();
    super.dispose();
  }

  onSelection(bool selected, Role role) {
    setState(() {
      if (selected) {
        _selectedRoles.add(role);
      } else {
        _selectedRoles.remove(role);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return ContentDialog(
      title: const Text('Create user'),
      content: Form(
        key: _formKey,
        child: Column(
          children: [
            Container(
              padding: const EdgeInsets.only(bottom: 8),
              child: TextFormBox(
                  header: "The Name of the User:",
                  autovalidateMode: AutovalidateMode.always,
                  controller: _nameController,
                  validator: (text) {
                    if (text == null || text.isEmpty) {
                      return 'Provide an name for the user';
                    }
                    return null;
                  }),
            ),
            Container(
              padding: const EdgeInsets.only(top: 8, bottom: 8),
              child: TextFormBox(
                  header: "The Email of the User:",
                  autovalidateMode: AutovalidateMode.always,
                  controller: _emailController,
                  validator: (text) {
                    if (text == null || text.isEmpty) {
                      return 'Provide an email for the user';
                    }
                    // Check if the email is valid
                    if (!text.contains('@')) {
                      return 'Provide a valid email';
                    }
                    return null;
                  }),
            ),
            Container(
              padding: const EdgeInsets.only(top: 8, bottom: 8),
              child: TextFormBox(
                  header: "The Password of the User:",
                  autovalidateMode: AutovalidateMode.always,
                  controller: _passwordController,
                  validator: (text) {
                    if (text == null || text.isEmpty) {
                      return 'Provide an password for the user';
                    }
                    return null;
                  }),
            ),
            // A multiselect list of roles
            Column(
              children: [
                Container(
                  padding: const EdgeInsets.only(top: 8, bottom: 4),
                  alignment: Alignment.centerLeft,
                  child: const Text("Select Roles:"),
                ),
                SizedBox(
                  height: 100,
                  width: 380,
                  child: material.Card(
                    child: BlocBuilder(
                      bloc: BlocProvider.of<RoleBloc>(context),
                      builder: (context, state) {
                        if (state is RoleLoadingState) {
                          return const LoadingIndicator();
                        } else if (state is RoleListErrorstate) {
                          return const LoadingIndicator();
                        } else if (state is RoleLoadedState) {
                          return Wrap(
                            direction: Axis.horizontal,
                            children: state.roles
                                .map((role) => UserCustomChip(
                                    role: role,
                                    isSelected: _selectedRoles.contains(role),
                                    mySelection: onSelection))
                                .toList(),
                          );
                        } else {
                          return const LoadingIndicator();
                        }
                      },
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
      actions: [
        Button(
          child: const Text('Cancel'),
          onPressed: () {
            // Dismiss the dialog
            Navigator.of(context).pop(false);
          },
        ),
        // Disable the button if the form is invalid
        Button(
          child: const Text('Save'),
          onPressed: () {
            // Validate and save the entry
            if (_formKey.currentState != null &&
                _formKey.currentState!.validate()) {
              // Dismiss the dialog
              User user = User(
                name: _nameController.text,
                email: _emailController.text,
                password: _passwordController.text,
                roles: _selectedRoles,
              );
              // Add the Created User to the User Bloc
              BlocProvider.of<UserBloc>(context).add(CreateUser(user));
              Navigator.of(context).pop(true);
            }
          },
        ),
      ],
    );
  }
}

import 'package:admin_ui/model/role.dart';
import 'package:fluent_ui/fluent_ui.dart';
import 'package:flutter/material.dart' as material;

class UserCustomChip extends StatelessWidget {
  const UserCustomChip({
    Key? key,
    required this.role,
    required this.isSelected,
    required this.mySelection,
  }) : super(key: key);

  final Role role;
  final bool isSelected;
  final Function mySelection;

  @override
  Widget build(BuildContext context) {
    return material.Container(
      padding: const EdgeInsets.all(3),
      child: material.FilterChip(
        selected: isSelected,
        backgroundColor: Colors.teal,
        avatar: material.CircleAvatar(
          backgroundColor: Colors.blue,
          child: Text(
            role.name[0].toUpperCase(),
            style: const TextStyle(color: Colors.white),
          ),
        ),
        label: Text(
          role.name,
        ),
        selectedColor: Colors.purple,
        onSelected: (bool selected) {
          mySelection(selected, role);
        },
      ),
    );
  }
}

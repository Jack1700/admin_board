import 'package:admin_ui/screens/user_screen/UserCreateDialog.dart';
import 'package:admin_ui/screens/user_screen/UserList.dart';
import 'package:fluent_ui/fluent_ui.dart';
import 'package:flutter/material.dart' as material;

class UserMain extends StatelessWidget {
  const UserMain({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScaffoldPage(
      // The Header with the title and the add user button
      header: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const Padding(
            padding: EdgeInsets.only(left: 16),
            child: Text(
              "Users",
              style: TextStyle(fontSize: 48),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 16),
            child: material.ElevatedButton(
              child: const Text("Add"),
              onPressed: () {
                showDialog<bool>(
                  context: context,
                  builder: (context) => const UserCreateDialog(),
                );
              },
            ),
          )
        ],
      ),
      // The User Data Table
      content: Container(
        padding: const EdgeInsets.all(16),
        child: const Center(
          child: UserList(),
        ),
      ),
    );
  }
}

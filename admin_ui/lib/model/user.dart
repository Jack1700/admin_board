import 'dart:convert';

import 'package:collection/collection.dart';

import 'role.dart';

// A Data class to hold the data of a user
class User {
  final String? id;
  final String name;
  final String email;
  final String? password;
  final List<Role> roles;
  User({
    this.id,
    required this.name,
    required this.email,
    this.password,
    required this.roles,
  });

  User copyWith({
    String? id,
    String? name,
    String? email,
    String? password,
    List<Role>? roles,
  }) {
    return User(
      id: id ?? this.id,
      name: name ?? this.name,
      email: email ?? this.email,
      password: password ?? this.password,
      roles: roles ?? this.roles,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'email': email,
      'password': password,
      'roles': roles.map((x) => x.toMap()).toList(),
    };
  }

  factory User.fromMap(Map<String, dynamic> map) {
    return User(
      id: map['id'],
      name: map['name'] ?? '',
      email: map['email'] ?? '',
      password: map['password'],
      roles: List<Role>.from(map['roles']?.map((x) => Role.fromMap(x))),
    );
  }

  String toJson() => json.encode(toMap());

  factory User.fromJson(Map<String, dynamic> json) {
    var _id = json['id'];
    var _name = json['name'];
    var _email = json['email'];
    var roles = <Role>[];
    if (json['roles'] != null) {
      List<dynamic> _roles = json['roles'];
      for (var role in _roles) {
        roles.add(Role.fromMap(role));
      }
    }
    return User(
      id: _id,
      name: _name,
      email: _email,
      roles: roles,
    );
  }

  @override
  String toString() {
    return 'User(id: $id, name: $name, email: $email, password: $password, roles: $roles)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
    final listEquals = const DeepCollectionEquality().equals;

    return other is User &&
        other.id == id &&
        other.name == name &&
        other.email == email &&
        other.password == password &&
        listEquals(other.roles, roles);
  }

  @override
  int get hashCode {
    return id.hashCode ^
        name.hashCode ^
        email.hashCode ^
        password.hashCode ^
        roles.hashCode;
  }
}

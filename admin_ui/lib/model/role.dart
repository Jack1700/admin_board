import 'dart:convert';

import 'package:collection/collection.dart';

import 'rule.dart';

class Role {
  final String? id;
  final String name;
  final String description;
  final List<Rule> rules;
  Role({
    this.id,
    required this.name,
    required this.description,
    required this.rules,
  });

  Role copyWith({
    String? id,
    String? name,
    String? description,
    List<Rule>? rules,
  }) {
    return Role(
      id: id ?? this.id,
      name: name ?? this.name,
      description: description ?? this.description,
      rules: rules ?? this.rules,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'description': description,
      'rules': rules.map((x) => x.toMap()).toList(),
    };
  }

  factory Role.fromMap(Map<String, dynamic> map) {
    List<Rule> rules = [];
    if (map['rules'] != null) {
      var _rules = map['rules'];
      for (var rule in _rules) {
        //check if rule data was populated
        if (rule is Map) {
          rules.add(Rule.fromJson(json.encode(rule)));
        }
      }
    }
    return Role(
      id: map['id'] ?? map['_id'],
      name: map['name'] ?? '',
      description: map['description'] ?? '',
      rules: rules,
    );
  }

  String toJson() => json.encode(toMap());

  factory Role.fromJson(Map<String, dynamic> json) {
    var _id = json['id'];
    var _name = json['name'];
    var _description = json['description'];
    var rules = <Rule>[];
    if (json['rules'] != null) {
      List<dynamic> _rules = json['rules'];
      for (var rule in _rules) {
        rules.add(Rule.fromMap(rule));
      }
    }
    return Role(
      id: _id,
      name: _name,
      description: _description,
      rules: rules,
    );
  }

  @override
  String toString() {
    return 'Role(id: $id, name: $name, description: $description, rules: $rules)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
    final listEquals = const DeepCollectionEquality().equals;

    return other is Role &&
        other.id == id &&
        other.name == name &&
        other.description == description &&
        listEquals(other.rules, rules);
  }

  @override
  int get hashCode {
    return id.hashCode ^ name.hashCode ^ description.hashCode ^ rules.hashCode;
  }
}

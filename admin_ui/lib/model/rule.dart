import 'dart:convert';

class Rule {
  final String? id;
  final String name;
  final String description;
  Rule({
    this.id,
    required this.name,
    required this.description,
  });

  Rule copyWith({
    String? id,
    String? name,
    String? description,
  }) {
    return Rule(
      id: id ?? this.id,
      name: name ?? this.name,
      description: description ?? this.description,
    );
  }

  Map<String, dynamic> toMap() {
    if (id == null) {
      return {
        'name': name,
        'description': description,
      };
    }
    return {
      'id': id,
      'name': name,
      'description': description,
    };
  }

  factory Rule.fromMap(Map<String, dynamic> map) {
    return Rule(
      id: map['id'] ?? map['_id'],
      name: map['name'] ?? '',
      description: map['description'] ?? '',
    );
  }

  String toJson() => json.encode(toMap());

  factory Rule.fromJson(String source) => Rule.fromMap(json.decode(source));

  @override
  String toString() => 'Rule(id: $id, name: $name, description: $description)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Rule &&
        other.id == id &&
        other.name == name &&
        other.description == description;
  }

  @override
  int get hashCode => id.hashCode ^ name.hashCode ^ description.hashCode;
}

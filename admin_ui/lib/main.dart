import 'package:admin_ui/bloc/role_bloc.dart';
import 'package:admin_ui/bloc/user_bloc.dart';
import 'package:admin_ui/screens/role_screen/RoleMain.dart';
import 'package:admin_ui/screens/rule_screen/RuleMain.dart';
import 'package:admin_ui/screens/user_screen/UserMain.dart';
import 'package:admin_ui/services/role_service.dart';
import 'package:admin_ui/services/rule_service.dart';
import 'package:admin_ui/services/user_service.dart';
import 'package:fluent_ui/fluent_ui.dart';
import 'package:flutter/material.dart' as material;
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/rule_bloc.dart';

void main() {
  // It its usefull to hold the Provider for the Blocs at the root of the app
  runApp(MultiBlocProvider(providers: [
    BlocProvider<RuleBloc>(
      create: (context) => RuleBloc(ruleService: RuleService()),
    ),
    BlocProvider<RoleBloc>(
      create: (context) => RoleBloc(roleService: RoleService()),
    ),
    BlocProvider<UserBloc>(
      create: (context) => UserBloc(userService: UserService()),
    ),
  ], child: const MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return FluentApp(
      title: 'Admin UI',
      theme:
          ThemeData(activeColor: Colors.magenta, brightness: Brightness.dark),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({
    Key? key,
  }) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  // Indicates the current selected tab
  int page = 0;

  // Fetch the data from the server for all the Blocs
  @override
  void initState() {
    super.initState();
    BlocProvider.of<UserBloc>(context).add(FetchUsers());
    BlocProvider.of<RoleBloc>(context).add(FetchRoles());
    BlocProvider.of<RuleBloc>(context).add(FetchRules());
  }

  @override
  Widget build(BuildContext context) {
    return NavigationView(
      appBar: const NavigationAppBar(
        title: Text("Fluent Design App Bar"),
      ),
      content: NavigationBody(
        index: page,
        children: const [
          UserMain(),
          RoleMain(),
          RuleMain(),
        ],
      ),
      pane: NavigationPane(
          displayMode: PaneDisplayMode.auto,
          selected: page,
          onChanged: (_newPage) {
            setState(() {
              page = _newPage;
            });
          },
          items: [
            PaneItem(
                icon: const Icon(
                  material.Icons.person,
                ),
                title: const Text("Users")),
            PaneItem(
                icon: const Icon(
                  material.Icons.list_alt,
                ),
                title: const Text("Roles")),
            PaneItem(
                icon: const Icon(material.Icons.rule_folder),
                title: const Text("Rules"))
          ]),
    );
  }
}

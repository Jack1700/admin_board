import 'package:admin_ui/model/role.dart';
import 'package:admin_ui/services/role_service.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

abstract class RoleEvent {}

class FetchRoles extends RoleEvent {}

class UpdateRole extends RoleEvent {
  final Role role;
  UpdateRole(this.role);
}

class CreateRole extends RoleEvent {
  final Role role;
  CreateRole(this.role);
}

class DeleteRole extends RoleEvent {
  final Role role;
  DeleteRole(this.role);
}

abstract class RoleState extends Equatable {
  @override
  List<Object?> get props => [];
}

class RoleLoadingState extends RoleState {}

class RoleInitialState extends RoleState {}

class RoleLoadedState extends RoleState {
  final List<Role> roles;
  RoleLoadedState({required this.roles});
}

class RoleListErrorstate extends RoleState {
  final error;
  RoleListErrorstate({this.error});
}

class RoleBloc extends Bloc<RoleEvent, RoleState> {
  final RoleService roleService;
  late List<Role> listRole;
  RoleBloc({required this.roleService}) : super(RoleInitialState()) {
    on<FetchRoles>((event, emit) async {
      emit(RoleLoadingState());
      await roleService.getRoles().then((value) {
        listRole = value;
        emit(RoleLoadedState(roles: listRole));
      }).catchError((error) {
        emit(RoleListErrorstate(error: error));
      });
    });
    on<UpdateRole>((event, emit) async {
      emit(RoleLoadingState());
      await roleService.updateRole(event.role).then((value) {
        // replcae the role in the list
        var index =
            listRole.indexWhere((element) => element.id == event.role.id);
        listRole[index] = event.role;
        emit(RoleLoadedState(roles: listRole));
      }).catchError((error) {
        emit(RoleListErrorstate(error: error));
      });
    });
    on<CreateRole>((event, emit) async {
      emit(RoleLoadingState());
      await roleService.createRole(event.role).then((newRole) {
        listRole.add(newRole);
        emit(RoleLoadedState(roles: listRole));
      }).catchError((error) {
        emit(RoleListErrorstate(error: error));
      });
    });
    on<DeleteRole>((event, emit) async {
      if (event.role.id != null) {
        emit(RoleLoadingState());
        await roleService.deleteRole(event.role.id!).then((value) {
          listRole.removeWhere((element) => element.id == event.role.id);
          emit(RoleLoadedState(roles: listRole));
        }).catchError((error) {
          emit(RoleListErrorstate(error: error));
        });
      } else {
        emit(RoleListErrorstate(error: "Role id is null"));
      }
    });
  }
}

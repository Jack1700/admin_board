import 'package:admin_ui/model/user.dart';
import 'package:admin_ui/services/user_service.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

// All the events that can be emitted by the user_bloc
abstract class UserEvent {}

class FetchUsers extends UserEvent {}

class UpdateUser extends UserEvent {
  final User user;
  UpdateUser(this.user);
}

class CreateUser extends UserEvent {
  final User user;
  CreateUser(this.user);
}

class DeleteUser extends UserEvent {
  final User user;
  DeleteUser(this.user);
}

// All the states that can be emitted by the user_bloc

abstract class UserState extends Equatable {
  @override
  List<Object?> get props => [];
}

class UserLoadingState extends UserState {}

class UserInitialState extends UserState {}

class UserLoadedState extends UserState {
  final List<User> users;
  UserLoadedState({required this.users});
}

class UserListErrorstate extends UserState {
  final error;
  UserListErrorstate({this.error});
}

// The user_bloc class

class UserBloc extends Bloc<UserEvent, UserState> {
  final UserService userService;
  late List<User> listUser;
  UserBloc({required this.userService}) : super(UserInitialState()) {
    on<FetchUsers>((event, emit) async {
      emit(UserLoadingState());
      await userService.getUsers().then((value) {
        listUser = value;
        emit(UserLoadedState(users: listUser));
      }).catchError((error) {
        emit(UserListErrorstate(error: error));
      });
    });
    on<UpdateUser>((event, emit) async {
      emit(UserLoadingState());
      await userService.updateUser(event.user).then((value) {
        // replcae the user in the list
        var index =
            listUser.indexWhere((element) => element.id == event.user.id);
        listUser[index] = event.user;
        emit(UserLoadedState(users: listUser));
      }).catchError((error) {
        emit(UserListErrorstate(error: error));
      });
    });
    on<CreateUser>((event, emit) async {
      emit(UserLoadingState());
      await userService.createUser(event.user).then((newUser) {
        listUser.add(newUser);
        emit(UserLoadedState(users: listUser));
      }).catchError((error) {
        emit(UserListErrorstate(error: error));
      });
    });
    on<DeleteUser>((event, emit) async {
      if (event.user.id != null) {
        emit(UserLoadingState());
        await userService.deleteUser(event.user.id!).then((value) {
          listUser.removeWhere((element) => element.id == event.user.id);
          emit(UserLoadedState(users: listUser));
        }).catchError((error) {
          emit(UserListErrorstate(error: error));
        });
      } else {
        emit(UserListErrorstate(error: "User id is null"));
      }
    });
  }
}

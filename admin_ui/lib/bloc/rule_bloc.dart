import 'package:admin_ui/model/rule.dart';
import 'package:admin_ui/services/rule_service.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

abstract class RuleEvent {}

class FetchRules extends RuleEvent {}

class UpdateRule extends RuleEvent {
  final Rule rule;
  UpdateRule(this.rule);
}

class CreateRule extends RuleEvent {
  final Rule rule;
  CreateRule(this.rule);
}

class DeleteRule extends RuleEvent {
  final Rule rule;
  DeleteRule(this.rule);
}

abstract class RuleState extends Equatable {
  @override
  List<Object?> get props => [];
}

class RuleLoadingState extends RuleState {}

class RuleInitialState extends RuleState {}

class RuleLoadedState extends RuleState {
  final List<Rule> rules;
  RuleLoadedState({required this.rules});
}

class RuleListErrorstate extends RuleState {
  final error;
  RuleListErrorstate({this.error});
}

class RuleBloc extends Bloc<RuleEvent, RuleState> {
  final RuleService ruleService;
  late List<Rule> listRule;
  RuleBloc({required this.ruleService}) : super(RuleInitialState()) {
    on<FetchRules>((event, emit) async {
      emit(RuleLoadingState());
      await ruleService.getRules().then((value) {
        listRule = value;
        emit(RuleLoadedState(rules: listRule));
      }).catchError((error) {
        emit(RuleListErrorstate(error: error));
      });
    });
    on<UpdateRule>((event, emit) async {
      emit(RuleLoadingState());
      await ruleService.updateRule(event.rule).then((value) {
        // replcae the rule in the list
        var index =
            listRule.indexWhere((element) => element.id == event.rule.id);
        listRule[index] = event.rule;
        emit(RuleLoadedState(rules: listRule));
      }).catchError((error) {
        emit(RuleListErrorstate(error: error));
      });
    });
    on<CreateRule>((event, emit) async {
      emit(RuleLoadingState());
      await ruleService.createRule(event.rule).then((newRule) {
        listRule.add(newRule);
        emit(RuleLoadedState(rules: listRule));
      }).catchError((error) {
        emit(RuleListErrorstate(error: error));
      });
    });
    on<DeleteRule>((event, emit) async {
      if (event.rule.id != null) {
        emit(RuleLoadingState());
        await ruleService.deleteRule(event.rule.id!).then((value) {
          listRule.removeWhere((element) => element.id == event.rule.id);
          emit(RuleLoadedState(rules: listRule));
        }).catchError((error) {
          emit(RuleListErrorstate(error: error));
        });
      } else {
        emit(RuleListErrorstate(error: "Rule id is null"));
      }
    });
  }
}

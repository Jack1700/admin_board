<div id="top"></div>


<br />
<div align="center">
  <h3 align="center">Admin Dashboard</h3>

  <p align="center">
    A simple dashboard for creating rights, roles and users!
  </p>
</div>

## Getting Started

The project is divided into two parts, one is the backend with a Node.JS server under the folder server. The frontend consisting of a Flutter Fluent UI for desktop applications and is in the admin_ui folder.

In the backend it was tried to separate between route controllers and models, controllers take over the heart and are responsible for updating the data in MongoDB. All controllers are tested with the help of Mocha and Chai and support the basic CRUD operations. The code is self-documenting using Swagger Ui, which means that under the ip of the server and the public paths docs a public documentation of all server endpoints is available. This is self updating. The server runs with nodman and is thus protected against crashes. There is a central error handling over which not wanted server errors could be filtered.

Flutter works in the front, together with Microsoft's Fluent UI. It is possible to launch the app on Linux as well as on the web (chrome). It should also be possible to build the app as a Windows application, but this has not yet been tested. The app consists of three screens, rules, roles, users, these are always similar in structure and possess only small deviations. The code for the user screen is documented as an example. For all three there is a data class, a service and a bloc. The data class is responsible for encoding or decoding the data and ensures a persistent data structure. The service is responsible for the communication with the server and performs the basic CRUD operations. The Bloc provides the state management. Together with a provider it ensures persistent states between different screens. For Example create add a new Rule and then goto the Role Screen to create a new Role, the rule you createt is already there.



### Installation

1. Install MongoDb from [https://www.mongodb.com/](https://mongodb.com)
2. Start a local MongoDB
3. Install all node packages 
4. cd server and then npm install
5. check if all tests are running correct 
6. npm test
7. if all 16 test passed, start the server
8. npm start
9. Install Flutter and Dart [https://flutter.dev/](https://flutter.dev/)
10. Install the VS COde Extension for Flutter and Dart
11. open the admin_ui folder in VS Code
12. Install all Flutter packages
13. open the pubspec.yaml and get all packages, also possible with the command flutter pub get
14. run the flutter app as an linux desktop app or as an web app





### Built With

* [TypeScript](https://www.typescriptlang.org/)
* [NodeJs](https://nodejs.org/)
* [Express](https://expressjs.com/)
* [Mongoose](https://mongoosejs.com/)
* [Swagger](https://swagger.io/)
* [Mocha](https://mochajs.org/)
* [Chai](https://www.chaijs.com/)
* [Bcrypt](https://www.npmjs.com/package/bcrypt)

* [Dart](https://dart.dev/)
* [Flutter](https://flutter.dev/)
* [Bloc](https://bloclibrary.dev/#/)
* [Fluent UI](https://pub.dev/packages/fluent_ui)




## Roadmap

- [ ] Backend
    - [x] Commit add user route controller and error handling
    - [x] Express, Swagger, Nodeman
    - [x] Bcrypt
    - [ ] Auth JWT, Passport
    - [x] Data Classes
    - [x] Routes
- [ ] Frontend
    - [x] Fluent
    - [x] Bloc
    - [ ] Offline Mode

<p align="right">(<a href="#top">back to top</a>)</p>



import { Get, Post, Patch, Delete, Route, Body } from "tsoa";
import { IRule, Rule, IRoleResponse } from "../models/rule";
import { BaseError } from "../ErrorHandling"

// Rule Controller
@Route("rule")
export default class RuleController {
    //return the rule with the given id get /rule/:id
    @Get("{id}")
    public async getRule(id: string): Promise<IRoleResponse> {
        //find the rule with the given id
        const rule = await Rule.findById(id);
        // check if rule exists
        if (!rule) {
            throw new BaseError("Rule not found", 404, null, true);
        }
        //return the rule
        return {
            id: String(rule._id),
            name: rule.name,
            description: rule.description
        };
    }

    //return all rules
    @Get()
    public async getRules(): Promise<IRoleResponse[]> {
        //find all rules
        const rules = await Rule.find();
        //check if rules exists
        if (!rules) {
            throw new BaseError("Rules not found", 404, null, true);
        }
        //return the rules
        return rules.map(rule => {
            return {
                id: String(rule._id),
                name: rule.name,
                description: rule.description
            };
        });
    }

    //create a new rule
    @Post()
    public async createRule(@Body() request: IRule): Promise<IRoleResponse> {
        //create a new rule
        const rule = new Rule({
            name: request.name,
            description: request.description
        });
        //save the rule
        await rule.save().catch(err => {
            throw new BaseError(err.message, 400, err, true);
        });
        //return the rule
        return {
            id: String(rule._id),
            name: rule.name,
            description: rule.description
        };
    }

    //update a rule
    @Patch("{id}")
    public async updateRule(id: string, @Body() {
        name,
        description
    }: IRule): Promise<IRoleResponse> {
        //find the rule with the given id
        const rule = await Rule.findById(id);
        // check if rule exists
        if (!rule) {
            throw new BaseError("Rule not found", 404, null, true);
        }
        //update the rule
        rule.name = name;
        rule.description = description;
        //save the rule
        await rule.save().catch(err => {
            throw new BaseError(err.message, 400, err, true);
        });
        //return the rule
        return {
            id: String(rule._id),
            name: rule.name,
            description: rule.description
        };
    }

    //delete a rule
    @Delete("{id}")
    public async deleteRule(id: string): Promise<void> {
        //find the rule with the given id
        const rule = await Rule.findById(id);
        //check if rule exists
        if (!rule) {
            throw new BaseError("Rule not found", 404, null, true);
        }
        //delete the rule
        await Rule.deleteOne({ _id: id }).catch(err => {
            throw new BaseError(err.message, 400, err, true);
        });
        return;
    }
}
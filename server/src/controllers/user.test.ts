import { expect } from 'chai';
import 'mocha';
import { User , IUser} from "../models/user";
import UserController from "./user";
import {connect} from "mongoose";
import { Role } from '../models/role';
import { Rule } from '../models/rule';

before(function (done) {
    connect('mongodb://localhost/test', done);
  });

// mongoose.connection.collections.test.drop();

// Test that mocks the User model and test the UserController
describe('UserController', () => {
    // Drop the Users collection at the beginning
    before(async () => {
        await User.deleteMany({});
        await Role.deleteMany({});
        await Rule.deleteMany({});
    });

    // Mock the User model with two users
    before(async () => {
        try {
             // Create some rules
             let rule1 = new Rule({
                _id: '5b0fa4dae9701e27748b4160',
                name: 'Create',
                description: 'Can create a new rule'
            });
            let rule2 = new Rule({
                _id: '5a0fa4dae9701f27748b4568',
                name: 'Read',
                description: 'Can read a rule'
            });
            await rule1.save()
            await rule2.save()            
            // Create some new roles
            let role1 = new Role({
                _id: '5b0fa4dae9701e27748b4161',
                name: 'Admin',
                description: 'Can create a new rule, delete a rule, update a rule, read a rule',
                rules: [rule1, rule2]

            });
            let role2 = new Role({
                _id: '5a0fa4dae9701e27748b4568',
                name: 'SuperUser',
                description: 'Can read all rules',
                rules: [rule2]
            });
            await role1.save()
            await role2.save()
            // Create some new user
            let user1 = new User({
                _id: '5a0fa4dae9701e27748b4567',
                name: 'John Doe',
                email: 'test1@mail.de',
                password: 'test',
                roles: [role2]
            });
            let user2 = new User({
                _id: '41224d776a326fb40f000001',
                name: 'Jane Doe',
                email: 'test2@mail.de',
                password: 'test',
                roles: [role2]
            });
            await user1.save()
            await user2.save()
            return
        } catch (error) {
            throw new Error(error)
        }

    });
    
    // Test the getUser function
    it('should return the user with the given id', async () => {
            const controller = new UserController();
            const response = await controller.getUser('5a0fa4dae9701e27748b4567');
            // check if the respone is not null
            expect(response).to.not.be.null;
            expect(String(response.id)).to.equal('5a0fa4dae9701e27748b4567', 'id should be 5a0fa4dae9701e27748b4567');
            expect(response.name).to.equal('John Doe', 'name should be John Doe');
            expect(response.email).to.equal('test1@mail.de', 'email should be test1@mail.de');
            expect(String(response.roles[0].name)).to.equal('SuperUser', 'roles[0] should be SuperUser');
    });

    // Test the getUsers function
    it('should return all users', async () => {
        const controller = new UserController();
        const response = await controller.getUsers();
        expect(response.length).to.equal(2, 'response should have 2 users');
    });


    // Test the createUser function
    it('should create a new user', async () => {
        const controller = new UserController();
        const role = await Role.findOne({name: 'Admin'});
        var request: IUser = {
                name: 'John Doe',
                email: 'test3@mail.com',
                password: 'test',
                roles: [role]
            };
        const response = await controller.createUser(request);
        expect(response.id).to.not.be.null;
        expect(response.name).to.equal('John Doe', 'name should be John Doe');
        expect(response.email).to.equal('test3@mail.com', 'email should be test3@mail.com');
        expect(String(response.roles[0].name)).to.equal('Admin', 'roles[0] should be Admin');
    });

    // Test updateUser function
    it('should update the user with the given id', async () => {
        const controller = new UserController();
        const role = await Role.findOne({name: 'Admin'});
        const request: IUser = {
            name: 'John Doe',
            email: 'test4@mail.com',
            password: 'test',
            roles: [role]
        };
        const response = await controller.updateUser('5a0fa4dae9701e27748b4567', request);
        expect(response.id).to.not.be.null;
        expect(response.name).to.equal('John Doe', 'name should be John Doe');
        expect(response.email).to.equal('test4@mail.com', 'email should be test4@mail.com');
        expect(String(response.roles[0].name)).to.equal('Admin', 'roles[0] should be Admin');
    });

    // Test the deleteUser function
    it('should delete the user with the given id', async () => {
        const controller = new UserController();
        await controller.deleteUser('41224d776a326fb40f000001');
        controller.getUser('41224d776a326fb40f000001').catch(err => {
            expect(err.status).to.equal(404, 'status should be 404');
        });
    });
});




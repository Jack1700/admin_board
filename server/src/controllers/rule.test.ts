import { expect } from 'chai';
import 'mocha';
import { Rule, IRule } from "../models/rule";
import RuleController from "./rule";
import { connect } from "mongoose";
import { BaseError } from '../ErrorHandling';

// Test the RuleController
describe('RuleController', () => {
    // Drop the Rules collection at the beginning
    before(async () => {
        await Rule.deleteMany({});
    });

    // Mock the Rule model with two rules
    before(async () => {
        try {
            let rule1 = new Rule({
                _id: '5a0fa4dae9701e27748b4567',
                name: 'Create',
                description: 'Can create a new rule'
            });
            let rule2 = new Rule({
                _id: '5a0fa4dae9701e27748b4568',
                name: 'Read',
                description: 'Can read a rule'
            });
            await rule1.save()
            await rule2.save()
            return
        }
        catch (error) {
            throw new Error(error)
        }
    });

    // Test the getRule function
    it('should return the rule with the given id', async () => {
        const controller = new RuleController();
        const response = await controller.getRule('5a0fa4dae9701e27748b4567');
        // check if the respone is not null
        expect(response).to.not.be.null;
        expect(String(response.id)).to.equal('5a0fa4dae9701e27748b4567', 'id should be 5a0fa4dae9701e27748b4567');
        expect(response.name).to.equal('Create', 'name should be Create');
        expect(response.description).to.equal('Can create a new rule', 'description should be Can create a new rule');
    });

    // Test the getRules function
    it('should return all rules', async () => {
        const controller = new RuleController();
        const response = await controller.getRules();
        // check if the respone is not null
        expect(response).to.not.be.null;
        expect(response.length).to.equal(2, 'length should be 2');
    });

    // Test the createRule function
    it('should create a new rule', async () => {
        const controller = new RuleController();
        const response = await controller.createRule({
            name: 'Update',
            description: 'Can update a rule'
        });
        // check if the respone is not null
        expect(response).to.not.be.null;
        expect(response.name).to.equal('Update', 'name should be Update');
        expect(response.description).to.equal('Can update a rule', 'description should be Can update a rule');
    });

    // Test the updateRule function
    it('should update a rule', async () => {
        const controller = new RuleController();
        const response = await controller.updateRule('5a0fa4dae9701e27748b4567', {
            name: 'Delete',
            description: 'Can delete a rule'
        });
        // check if the respone is not null
        expect(response).to.not.be.null;
        expect(String(response.id)).to.equal('5a0fa4dae9701e27748b4567', 'id should be 5a0fa4dae9701e27748b4567');
        expect(response.name).to.equal('Delete', 'name should be Delete');
        expect(response.description).to.equal('Can delete a rule', 'description should be Can delete a rule');
    });

    // Test the deleteRule function
    it('should delete a rule', async () => {
        const controller = new RuleController();
        await controller.deleteRule('5a0fa4dae9701e27748b4567');
        // check if the rule is deleted
        await controller.getRule('5a0fa4dae9701e27748b4567').catch(err => {
            expect(err.httpCode).to.equal(404, 'httpCode should be 404');
            expect(err.name).to.equal('Rule not found', 'name should be Rule not found');
        });
    });
});
import { expect } from 'chai';
import 'mocha';
import { Role, IRole } from "../models/role";
import { Rule, IRule } from "../models/rule";
import RoleController from "./role";
import { connect } from "mongoose";
import { BaseError } from '../ErrorHandling';

// Test the RoleController
describe('RoleController', () => {
    // Drop the Rules and Roles collections at the beginning
    before(async () => {
        await Role.deleteMany({});
        await Rule.deleteMany({});
    });

    // Mock the Rule model with two rules
    before(async () => {
        try {
            // Create some rules
            let rule1 = new Rule({
                _id: '5a0fa4dae9701e27748b4567',
                name: 'Create',
                description: 'Can create a new rule'
            });
            let rule2 = new Rule({
                _id: '5a0fa4dae9701e27748b4568',
                name: 'Read',
                description: 'Can read a rule'
            });
            await rule1.save()
            await rule2.save()            
            // Create a new role
            let role1 = new Role({
                _id: '5a0fa4dae9701e27748b4567',
                name: 'Admin',
                description: 'Can create a new rule, delete a rule, update a rule, read a rule',
                rules: [rule1, rule2]

            });
            let role2 = new Role({
                _id: '5a0fa4dae9701e27748b4568',
                name: 'SuperUser',
                description: 'Can read all rules',
                rules: [rule2]
            });
            await role1.save()
            await role2.save()
            return
        }
        catch (error) {
            throw new Error(error)
        }
    });

    // Test the getRole function
    it('should return the role with the given id', async () => {
        const controller = new RoleController();
        const response = await controller.getRole('5a0fa4dae9701e27748b4567');
        // check if the respone is not null
        expect(response).to.not.be.null;
        expect(String(response.id)).to.equal('5a0fa4dae9701e27748b4567', 'id should be 5a0fa4dae9701e27748b4567');
        expect(response.name).to.equal('Admin', 'name should be Admin');
        expect(response.description).to.equal('Can create a new rule, delete a rule, update a rule, read a rule', 'description should be Can create a new rule, delete a rule, update a rule, read a rule');
    });

    // Test the getRoles function
    it('should return all roles', async () => {
        const controller = new RoleController();
        const response = await controller.getRoles();
        // check if the respone is not null
        expect(response).to.not.be.null;
        expect(response.length).to.equal(2, 'length should be 2');
    });

    // Test the createRole function
    it('should create a new role', async () => {
        const controller = new RoleController();
        const response = await controller.createRole({
            name: 'User',
            description: 'Can do nothing',
            rules: []
        });
        // check if the respone is not null
        expect(response).to.not.be.null;
        expect(response.name).to.equal('User', 'name should be User');
        expect(response.description).to.equal('Can do nothing', 'description should be Can do nothing');
    });

    // Test the updateRole function
    it('should update a role', async () => {
        const controller = new RoleController();
        const response = await controller.updateRole('5a0fa4dae9701e27748b4567', {
            name: 'Moderator',
            description: 'Can moderate things',
            rules: []
        });
        // check if the respone is not null
        expect(response).to.not.be.null;
        expect(response.name).to.equal('Moderator', 'name should be Moderator');
        expect(response.description).to.equal('Can moderate things', 'description should be Can moderate things');
    });

    // Test the deleteRole function
    it('should delete a role', async () => {
        const controller = new RoleController();
        await controller.deleteRole('5a0fa4dae9701e27748b4567');
        // check if the Role is deleted
        await controller.getRole('5a0fa4dae9701e27748b4567').catch(error => {
            expect(error).to.not.be.null;
            expect(error.httpCode).to.equal(404, 'httpCode should be 404');
        });
    });
});
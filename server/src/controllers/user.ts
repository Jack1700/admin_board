import { Get, Post, Patch, Delete, Route, Body} from "tsoa";
import { User, IUserResponse, IUser} from "../models/user";
import { BaseError } from "../ErrorHandling"
import  bcrypt from "bcrypt";

@Route("user")
export default class UserController {
    //return the user with the given id get /user/:id
    @Get("{id}")
    public async getUser(id: string): Promise<IUserResponse> {
        //find the user with the given id
        const user = await User.findById(id).populate("roles").catch(err => {
            throw new BaseError(err.message, 400, err, true);
        });
        // check if user exists
        if (!user) {
            throw new BaseError("User not found", 404, null, true);
        }
        //return the user
        return {
            id: String(user._id),
            name: user.name,
            email: user.email,
            roles: user.roles
        };
    }

    //return all users
    @Get()
    public async getUsers(): Promise<IUserResponse[]> {
        //find all users
        const users = await User.find().populate("roles").catch(err => {
            throw new BaseError(err.message, 400, err, true);
        });
        //check if users exists
        if (!users) {
            throw new BaseError("Users not found", 404, null, true);
        }
        //return the users
        return users.map(user => {
            return {
                id: String(user._id),
                name: user.name,
                email: user.email,
                roles: user.roles
            };
        });
    }

    //create a new user
    @Post()
    public async createUser(@Body() request: IUser): Promise<IUserResponse> {
        const hashedPassword = await bcrypt.hash(request.password, 12);
        //create a new user
        const user = new User({
            name: request.name,
            email: request.email,
            password: hashedPassword,
            roles: request.roles
        });
        //save the user
        await (await user.save()).populate("roles").catch(err => {
            throw new BaseError(err.message, 400, err, true);
        });
        //return the user
        return {
            id: String(user._id),
            name: user.name,
            email: user.email,
            roles: user.roles
        };
    }

    //delete the user with the given id
    @Delete("{id}")
    public async deleteUser(id: string): Promise<void> {
        //find the user with the given id
        const user = await User.findById(id);
        //check if user exists
        if (!user) {
            throw new BaseError("User not found", 404, null, true);
        }
        //delete the user and return the success message
        await User.deleteOne({ _id: id }).catch(err => {
            throw new BaseError(err.message, 400, err, true);
        });
        return;
    }

    //update the user with the given id
    @Patch("{id}")
    public async updateUser(id: string, @Body() request: IUser): Promise<IUserResponse> {
        //find the user with the given id
        const user = await User.findById(id).populate("roles").catch(err => {
            throw new BaseError(err.message, 400, err, true);
        });
        //update the user
        user.name = request.name;
        user.email = request.email;
        user.roles = request.roles;
        //save the user
        await user.save().catch(err => {
            throw new BaseError(err.message, 400, err, true);
        });
        //return the user
        return {
            id: String(user._id),
            name: user.name,
            email: user.email,
            roles: user.roles
        };
    }



}
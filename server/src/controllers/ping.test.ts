import PingController from "./ping";
import { expect } from 'chai';
import 'mocha';

describe('Pong function', () => {

  it('should return pong', async () => {
    const controller = new PingController();
    const response = await controller.getMessage();
    expect(response.message).to.equal("pong");
  });

});

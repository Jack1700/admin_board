import { Get, Post, Patch, Delete, Route, Body} from "tsoa";
import { Role, IRole, IRoleResponse, IRoleRequest} from "../models/role";
import { BaseError } from "../ErrorHandling"
import { Types } from "mongoose";
import { request } from "http";
import { Rule } from "../models/rule";

// Role Controller
@Route("role")
export default class RoleController {
    //return the role with the given id get /role/:id
    @Get("{id}")
    public async getRole(id: string): Promise<IRoleResponse> {
        //find the role with the given id
        const role = await Role.findById(id).populate("rules").catch(err => {
            throw new BaseError(err.message, 400, err, true);
        });
        // check if role exists
        if (!role) {
            throw new BaseError("Role not found", 404, null, true);
        }
        //return the role
        return {
            id: String(role._id),
            name: role.name,
            description: role.description,
            rules: role.rules
        };
    }

    //return all roles
    @Get()
    public async getRoles(): Promise<IRoleResponse[]> {
        //find all roles
        const roles = await Role.find().populate("rules");
        //check if roles exists
        if (!roles) {
            throw new BaseError("Roles not found", 404, null, true);
        }
        //return the roles
        return roles.map(role => {
            return {
                id: String(role._id),
                name: role.name,
                description: role.description,
                rules: role.rules
            };
        });
    }

    //create a new role
    @Post()
    public async createRole(@Body() request: IRole): Promise<IRoleResponse> {
        //create a new role
        const role = new Role({
            name: request.name,
            description: request.description,
            rules: request.rules
        });
        //save the role
        await (await role.save()).populate("rules").catch(err => {
            throw new BaseError(err.message, 400, err, true);
        });
        //return the role
        return {
            id: String(role._id),
            name: role.name,
            description: role.description,
            rules: role.rules
        };
    }

    //update a role
    @Patch("{id}")
    public async updateRole(id: string, @Body() request: IRole): Promise<IRoleResponse> {
        //find the role with the given id
        const role = await Role.findById(id);
        // check if role exists
        if (!role) {
            throw new BaseError("Role not found", 404, null, true);
        }
        //update the role
        role.name = request.name;
        role.description = request.description;
        role.rules = request.rules;
        //save the role
        await role.save().catch(err => {
            throw new BaseError(err.message, 400, err, true);
        });
        //return the role
        return {
            id: String(role._id),
            name: role.name,
            description: role.description,
            rules: role.rules
        };
    }

    //delete a role
    @Delete("{id}")
    public async deleteRole(id: string): Promise<IRoleResponse> {
        //find the role with the given id
        const role = await Role.findById(id);
        // check if role exists
        if (!role) {
            throw new BaseError("Role not found", 404, null, true);
        }
        //delete the role
        await role.remove().catch(err => {
            throw new BaseError(err.message, 400, err, true);
        });
        //return the role
        return {
            id: String(role._id),
            name: role.name,
            description: role.description,
            rules: role.rules
        };
    }

}
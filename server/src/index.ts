import express, { NextFunction, Request, Response, Application } from "express";
import morgan from "morgan";
import Router from "./routes";
import swaggerUi from "swagger-ui-express";
import cors from 'cors';
import { BaseError } from "./ErrorHandling";

const mongoose = require('mongoose');
const url = 'mongodb://127.0.0.1:27017/adminDB?directConnection=true&serverSelectionTimeoutMS=2000&appName=adminServer';

try {
  mongoose.connect(url);
  console.log(`MongoDB Connected: ${url}`);
} catch (err) {
  console.error(err);
}

const PORT = process.env.PORT || 8000;

const app: Application = express();

app.use(express.json());
app.use(morgan("tiny"));
app.use(express.static("public"));


app.use(cors())

app.use(
  "/docs",
  swaggerUi.serve,
  swaggerUi.setup(undefined, {
    swaggerOptions: {
      url: "/swagger.json",
    },
  })
);

app.use(Router);

function errorMiddleware(error: BaseError, request: Request, response: Response, next: NextFunction) {
  const status = error.httpCode || 500;
  const message = error.message || 'Something went wrong';
  response.status(status).send({ message });
}

app.use(errorMiddleware)

app.listen(PORT, () => {
  console.log("Server is running on port", PORT);
});
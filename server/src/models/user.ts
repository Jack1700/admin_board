import { model, Schema, Model, Document } from 'mongoose';
import { IRole } from './role';

export interface IUserResponse {
    id: string;
    name: string;
    email: string;
    roles: IRole[];
}

export interface IUser {
    name: string;
    email: string;
    password: string;
    roles: IRole[];
}

const UserSchema: Schema = new Schema({
    name: { type: String, required: true },
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    roles: [{ type: Schema.Types.ObjectId, ref: 'Role' }]
});

export const User: Model<IUser> = model('User', UserSchema);
    
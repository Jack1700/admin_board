import { model, Schema, Model, Document } from 'mongoose';
import { IRule } from './rule';

export interface IRoleResponse {
    id: string;
    name: string;
    description: string;
    rules: IRule[];
}

export interface IRoleRequest {
    name: string;
    description: string;
    rules: string[];
}

export interface IRole {
    name: string;
    description: string;
    rules: IRule[];
}

const RoleSchema: Schema = new Schema({
    name: { type: String, required: true },
    description: { type: String, required: true },
    rules: [{ type: Schema.Types.ObjectId, ref: 'Rule' }]
});

export const Role: Model<IRole> = model('Role', RoleSchema);
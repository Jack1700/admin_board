import { model, Schema, Model, Document } from 'mongoose';

export interface IRoleResponse {
    id: string;
    name: string;
    description: string;
}

export interface IRule {
    name: string;
    description: string;
  }

export interface IRuleDoc extends Document {
  name: string;
  description: string;
}

const RuleSchema: Schema = new Schema({
  name: { type: String, required: true },
  description: { type: String, required: true }
});

// Middleware to ensure that no Rule with active refs in Role is deleted
RuleSchema.pre('remove', async function(next) {
    const Role = require('.role');
    Role.find({ rules: this._id }).exec((err, roles) => {
        if (err) {
            next(err);
        } else {
            if (roles.length > 0) {
                next(new Error('Rule is referenced by a Role'));
            } else {
                next();
            }
        }
    });
});

export const Rule: Model<IRule> = model('Rule', RuleSchema);
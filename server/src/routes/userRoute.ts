import express, { response } from "express";
import UserController from "../controllers/user";
import { Role } from "../models/role";
import { IUser } from "../models/user";

const userRouter = express.Router();

// Basic CRUD routes for users
userRouter.get("/user", async (_req, res) => {
    const controller = new UserController();
    const response = await controller.getUsers();
    return res.send(response);
});

//get user by id
userRouter.get("/user/:id", async (req, res) => {
    const controller = new UserController();
    const response = await controller.getUser(req.params.id);
    return res.send(response);
});

//create a new user
userRouter.post("/user", async (req, res, next) => {
    const controller = new UserController();
    var roles = JSON.parse(req.body.roles);
    const _roles = []
    for (const role of roles) {
        var jsonRole = JSON.parse(role);
        _roles.push(new Role({_id: jsonRole.id, name: jsonRole.name, description: jsonRole.description}));
    }
    const request: IUser = {
        name: req.body.name,
        email: req.body.email,
        password: req.body.password,
        roles: _roles
    };
    controller.createUser(request)
        .then((response) => {
            return res.send(response);
        })
        .catch(err => {
            console.error(err);
            next(err);
        });
});

//update user by id
userRouter.put("/user/:id", async (req, res, next) => {
    const controller = new UserController();
    var roles = JSON.parse(req.body.roles);
    const _roles = []
    for (const role of roles) {
        var jsonRole = JSON.parse(role);
        _roles.push(new Role({_id: jsonRole.id, name: jsonRole.name, description: jsonRole.description}));
    }
    const request: IUser = {
        name: req.body.name,
        email: req.body.email,
        password: "",
        roles: _roles
    };
    controller.updateUser(req.params.id, request)
        .then((response) => {
            return res.send(response);
        })
        .catch(err => {
            console.error(err);
            next(err);
        });
});

//delete user by id
userRouter.delete("/user/:id", async (req, res, next) => {
    const controller = new UserController();
    controller.deleteUser(req.params.id)
        .then(() => {
            return res.sendStatus(200);
        })
        .catch(err => {
            console.error(err);
            next(err);
        });
});



export default userRouter;
import express, { response } from "express";
import RoleController from "../controllers/role";
import { IRole, IRoleRequest } from "../models/role";
import { Rule } from "../models/rule";

const roleRouter = express.Router();

// Basic CRUD routes for roles
roleRouter.get("/role", async (_req, res) => {
    const controller = new RoleController();
    const response = await controller.getRoles();
    return res.send(response);
});

//get role by id
roleRouter.get("/role/:id", async (req, res) => {
    const controller = new RoleController();
    const response = await controller.getRole(req.params.id);
    return res.send(response);
});

// create a new role
roleRouter.post("/role", async (req, res, next) => {
    const controller = new RoleController();
    var rules = JSON.parse(req.body.rules);
    const _roles = []
    for (const rule of rules) {
        var jsonRule = JSON.parse(rule);
        _roles.push(new Rule({_id: jsonRule.id, name: jsonRule.name, description: jsonRule.description}));
    }

    const request: IRole = {
        name: req.body.name,
        description: req.body.description,
        rules: _roles
    };
    controller.createRole(request)
        .then((response) => {
            return res.send(response);
        })
        .catch(err => {
            console.error(err);
            next(err);
        });
});

// update role by id
roleRouter.put("/role/:id", async (req, res, next) => {
    const controller = new RoleController();
    var rules = JSON.parse(req.body.rules);
    const _roles = []
    for (const rule of rules) {
        var jsonRule = JSON.parse(rule);
        _roles.push(new Rule({_id: jsonRule.id, name: jsonRule.name, description: jsonRule.description}));
    }
    const request: IRole = {
        name: req.body.name,
        description: req.body.description,
        rules: _roles
    };
    controller.updateRole(req.params.id, request)
        .then((response) => {
            return res.send(response);
        })
        .catch(err => {
            console.error(err);
            next(err);
        });
});

// delete role by id
roleRouter.delete("/role/:id", async (req, res, next) => {
    const controller = new RoleController();
    controller.deleteRole(req.params.id)
        .then(() => {
            return res.sendStatus(200);
        })
        .catch(err => {
            console.error(err);
            next(err);
        });
});

export default roleRouter;
import express, { response } from "express";
import RuleController from "../controllers/rule";
import { IRule } from "../models/rule";

const ruleRouter = express.Router();

// Basic CRUD routes for rules
ruleRouter.get("/rule", async (_req, res) => {
    const controller = new RuleController();
    const response = await controller.getRules();
    return res.send(response);
});

//get rule by id
ruleRouter.get("/rule/:id", async (req, res) => {
    const controller = new RuleController();
    const response = await controller.getRule(req.params.id);
    return res.send(response);
});

//create a new rule
ruleRouter.post("/rule", async (req, res, next) => {
    const controller = new RuleController();
    const request: IRule = req.body;
    controller.createRule(request)
        .then((response) => {
            return res.send(response);
        })
        .catch(err => {
            console.error(err);
            next(err);
        });
});

//update rule by id
ruleRouter.put("/rule/:id", async (req, res, next) => {
    const controller = new RuleController();
    const request: IRule = req.body;
    controller.updateRule(req.params.id, request)
        .then((response) => {
            return res.send(response);
        })
        .catch(err => {
            console.error(err);
            next(err);
        });
});

//delete rule by id
ruleRouter.delete("/rule/:id", async (req, res, next) => {
    const controller = new RuleController();
    controller.deleteRule(req.params.id)
        .then(() => {
            return res.sendStatus(200);
        })
        .catch(err => {
            console.error(err);
            next(err);
        });
});

export default ruleRouter;
import express from "express";
import PingController from "../controllers/ping";
import userRouter from "./userRoute";
import roleRouter from "./roleRoute";
import ruleRouter from "./ruleRote";

const router = express.Router();

router.get("/ping", async (_req, res) => {
  const controller = new PingController();
  const response = await controller.getMessage();
  return res.send(response);
});

// Forward user routes to the user router
router.use(userRouter);

// Forward role routes to the role router
router.use(roleRouter);

// Forward rules routes to the rules router
router.use(ruleRouter);



export default router;